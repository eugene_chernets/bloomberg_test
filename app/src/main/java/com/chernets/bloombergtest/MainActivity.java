package com.chernets.bloombergtest;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity implements RecyclerAdapter.ImageClickListener, RecyclerAdapter.GridScrollListener {

    private final static String TAG = MainActivity.class.getSimpleName();

    private final String url = "https://api.500px.com/v1/photos?feature=popular&consumer_key=wB4ozJxTijCwNuggJvPGtBGCRqaZVcF6jsrzUadF&page=";

    private ShareActionProvider shareActionProvider;
    private RecyclerAdapter recyclerAdapter;
    private RecyclerView recyclerView;
    private ArrayList<ImageItem> imageItems = new ArrayList<>();
    private RelativeLayout previewContainer;
    private TextView description;
    private ImageView preview;
    private FloatingActionButton shareButton;
    private int itemSide;
    private int currentPage = 0;
    private int currentItem = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preview = (ImageView) findViewById(R.id.preview);
        description = (TextView) findViewById(R.id.description);
        previewContainer = (RelativeLayout) findViewById(R.id.preview_container);
        previewContainer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP)
                    onBackPressed();
                return true;
            }
        });

        shareButton = (FloatingActionButton) findViewById(R.id.fab);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = imageItems.get(currentItem).getImageURL();
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Beautiful picture");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler);

        int spanCount = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ? 2 : 4;
        recyclerView.setLayoutManager(new GridLayoutManager(this, spanCount));

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        int margin = getResources().getDimensionPixelSize(R.dimen.grid_margin);
        itemSide = (width - (spanCount + 1) * margin) / spanCount + margin;

        if (connectionAlive())
            downloadData(currentPage);

        recyclerAdapter = new RecyclerAdapter(this, itemSide, imageItems, this, this);
        recyclerView.setAdapter(recyclerAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(connectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {
        unregisterReceiver(connectivityReceiver);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (previewContainer.getVisibility() == View.VISIBLE) {
            previewContainer.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    private boolean connectionAlive() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork == null || !activeNetwork.isConnectedOrConnecting() ||
                activeNetwork.getType() != ConnectivityManager.TYPE_WIFI) {
            showInternetConnectionDialog();
            return false;
        }
        return true;
    }

    private void showInternetConnectionDialog() {
        final Dialog dialog = new Dialog(this, R.style.AppTheme);
        View content = LayoutInflater.from(this).inflate(R.layout.dialog_layout, null);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(content);
        ((TextView) content.findViewById(R.id.dialog_message)).setText(getString(R.string.connect_to_wifi));
        Button buttonOk = (Button) content.findViewById(R.id.dialog_ok);
        buttonOk.setText(getResources().getString(R.string.settings));
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                dialog.dismiss();
            }
        });
        Button buttonCancel = (Button) content.findViewById(R.id.dialog_cancel);
        buttonCancel.setText(getResources().getString(R.string.cancel));
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                onBackPressed();
            }
        });
        dialog.show();
    }

    private void downloadData(int page) {
        JSONObject jsonObject = downloadJsonPage(page);
        if (jsonObject != null) {
            if (parseImageItems(jsonObject))
                refreshAdapter();
        }
        Log.d(TAG, "downloadBitmaps page " + page + " items count " + imageItems.size());
    }

    private void refreshAdapter() {
        if (recyclerAdapter != null && recyclerView != null && !recyclerView.isComputingLayout()) {
            recyclerAdapter.notifyDataSetChanged();
            Log.d(TAG, "notifyDataSetChanged");
        } else {
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            refreshAdapter();
                        }
                    });
                }
            }, 1000);
        }
    }

    private JSONObject downloadJsonPage(int page) {
        JSONObject jsonObject = null;
        try {
            JsonDownloader jsonDownloader = new JsonDownloader();
            jsonDownloader.execute(url + page);
            jsonObject = jsonDownloader.get();
        } catch (InterruptedException e) {
            Log.d(TAG, "InterruptedException " + e.getMessage());
            e.printStackTrace();
        } catch (ExecutionException e) {
            Log.d(TAG, "ExecutionException " + e.getMessage());
            e.printStackTrace();
        }
        return jsonObject;
    }


    private boolean parseImageItems(JSONObject jsonObject) {
        if (jsonObject == null)
            return false;
        try {
            JSONArray photosJsonArray = jsonObject.getJSONArray("photos");
            JSONObject photoJsonObject;
            for (int i = 0; i < photosJsonArray.length(); i++) {
                photoJsonObject = photosJsonArray.getJSONObject(i);
                Log.d(TAG, "photJsonObject " + photoJsonObject);
                imageItems.add(new ImageItem(photoJsonObject));
            }
        } catch (JSONException e) {
            Log.d(TAG, "JSONException " + e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public void imageClicked(int position) {
        try {
            currentItem = position;
            ImageItem item = imageItems.get(position);
            BitmapDownloader task = new BitmapDownloader();
            task.execute(item.getImageURL());
            Bitmap bitmap = task.get();
            previewContainer.setVisibility(View.VISIBLE);
            preview.setImageBitmap(bitmap);
            description.setText(getString(R.string.description, item.getImageName(),
                    item.getAuthorName(), item.getCameraModel()));
        } catch (InterruptedException e) {
            Log.d(TAG, "InterruptedException " + e.getMessage());
            e.printStackTrace();
        } catch (ExecutionException e) {
            Log.d(TAG, "ExecutionException " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void requestMoreImages() {
        downloadData(currentPage++);
    }

    private final BroadcastReceiver connectivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ConnectivityManager.CONNECTIVITY_ACTION.equals(intent.getAction())) {
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                if (imageItems.size() == 0 && netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                    downloadData(currentPage);
                }
            }
        }
    };
}

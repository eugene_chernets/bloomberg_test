package com.chernets.bloombergtest;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Created by evgeniych on 18.03.2016.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private final static String TAG = MainActivity.class.getSimpleName();

    private int itemSide;
    private ArrayList<ImageItem> imageItems;
    private ImageClickListener imageClickListener;
    private GridScrollListener gridScrollListener;

    public RecyclerAdapter(Context context, int itemSide, ArrayList<ImageItem> imageItems,
                           ImageClickListener imageClickListener, GridScrollListener gridScrollListener) {
        this.itemSide = itemSide;
        this.imageItems = imageItems;
        this.imageClickListener = imageClickListener;
        this.gridScrollListener = gridScrollListener;
    }

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder, parent, false);
        view.findViewById(R.id.holder_root).setLayoutParams(new LinearLayout.LayoutParams(itemSide, itemSide));
        return new ViewHolder(view, imageClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder position " + position + " total count " + imageItems.size());

        ImageItem item = imageItems.get(position);
        if (item.getBitmap() == null)
            try {
                BitmapDownloader task = new BitmapDownloader();
                task.execute(imageItems.get(position).getImageURL());
                item.setBitmap(miniaturizeBitmap(task.get()));
                holder.imageView.setImageBitmap(item.getBitmap());
            } catch (InterruptedException e) {
                Log.d(TAG, "InterruptedException " + e.getMessage());
                e.printStackTrace();
            } catch (ExecutionException e) {
                Log.d(TAG, "ExecutionException " + e.getMessage());
                e.printStackTrace();
            }
        else
            holder.imageView.setImageBitmap(item.getBitmap());

        holder.number.setText("" + position);
        holder.position = position;

        if (position == imageItems.size() - 1)
            gridScrollListener.requestMoreImages();
    }

    @Override
    public int getItemCount() {
        return imageItems.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView number;
        public ImageView imageView;
        public int position;

        public ViewHolder(View view, final ImageClickListener imageClickListener) {
            super(view);
            number = (TextView) view.findViewById(R.id.number);
            imageView = (ImageView) view.findViewById(R.id.image);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageClickListener.imageClicked(position);
                }
            });
        }
    }

    private Bitmap miniaturizeBitmap(Bitmap bitmap) {
        int outWidth;
        int outHeight;
        int maxSide = Math.max(bitmap.getWidth(), bitmap.getHeight());
        if (maxSide > itemSide) {
            if (bitmap.getWidth() == maxSide) {
                outWidth = itemSide;
                outHeight = bitmap.getHeight() * (itemSide / bitmap.getWidth());
            } else {
                outHeight = itemSide;
                outWidth = bitmap.getWidth() * (itemSide / bitmap.getHeight());
            }
            return Bitmap.createScaledBitmap(bitmap, outWidth, outHeight, true);
        } else {
            return bitmap;
        }
    }

    public interface ImageClickListener {
        void imageClicked(int position);
    }

    public interface GridScrollListener {
        void requestMoreImages();
    }
}

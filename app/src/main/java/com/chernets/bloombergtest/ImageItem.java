package com.chernets.bloombergtest;

import android.graphics.Bitmap;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by evgeniych on 22.03.2016.
 */
public class ImageItem {
    private String imageName;
    private String authorName;
    private String cameraModel;
    private String imageURL;
    private Bitmap bitmap;

    public ImageItem(String imageName, String authorName, String cameraModel, String imageURL) {
        this.imageName = imageName;
        this.authorName = authorName;
        this.cameraModel = cameraModel;
        this.imageURL = imageURL;
    }

    public ImageItem(JSONObject jsonObject) throws JSONException {
        this.imageName = jsonObject.getString("name");
        this.authorName = jsonObject.getJSONObject("user").getString("fullname");
        this.cameraModel = jsonObject.getString("camera");
        this.imageURL = jsonObject.getString("image_url");
    }

    public String getImageName() {
        return imageName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getCameraModel() {
        return cameraModel;
    }

    public String getImageURL() {
        return imageURL;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}

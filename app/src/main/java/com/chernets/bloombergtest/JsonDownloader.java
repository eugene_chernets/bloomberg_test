package com.chernets.bloombergtest;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by evgeniych on 22.03.2016.
 */
public class JsonDownloader extends AsyncTask<String, Void, JSONObject> {

    private final static String TAG = MainActivity.class.getSimpleName();

    @Override
    protected JSONObject doInBackground(String... params) {
        JSONObject jsonResult = null;
        HttpURLConnection httpURLConnection = null;
        try {
            Log.d(TAG, "requesting with url " + params[0]);

            URL url = new URL(params[0]);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            httpURLConnection.connect();

            Log.d(TAG, "response " + httpURLConnection.getResponseMessage());

            String result = "";
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader r = new BufferedReader(inputStreamReader);
            String line = r.readLine();
            while (line != null) {
                result += line;
                line = r.readLine();
            }
            Log.d(TAG, result);
            jsonResult = new JSONObject(result);
        } catch (MalformedURLException e) {
            Log.d(TAG, "MalformedURLException " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "IOException " + e.getMessage());
            Log.d(TAG, "IOException " + e.toString());
            e.printStackTrace();
        } catch (JSONException e) {
            Log.d(TAG, "JSONException " + e.getMessage());
        } finally {
            if (httpURLConnection != null)
                httpURLConnection.disconnect();
        }
        return jsonResult;
    }
}

package com.chernets.bloombergtest;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by evgeniych on 22.03.2016.
 */
public class BitmapDownloader extends AsyncTask<String, Void, Bitmap> {

    private final static String TAG = MainActivity.class.getSimpleName();

    @Override
    protected Bitmap doInBackground(String... params) {
        Bitmap bitmap = null;
        HttpURLConnection httpURLConnection = null;
        try {
            Log.d(TAG, "requesting with url " + params[0]);
            URL url = new URL(params[0]);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            httpURLConnection.connect();
            Log.d(TAG, "response " + httpURLConnection.getResponseMessage());
            bitmap = BitmapFactory.decodeStream(inputStream);
        } catch (MalformedURLException e) {
            Log.d(TAG, "MalformedURLException " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "IOException " + e.getMessage());
            Log.d(TAG, "IOException " + e.toString());
            e.printStackTrace();
        } finally {
            if (httpURLConnection != null)
                httpURLConnection.disconnect();
        }
        return bitmap;
    }

}
